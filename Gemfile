source 'https://rubygems.org'

gemspec

def ruby_version?(constraint)
  Gem::Dependency.new('', constraint).match?('', RUBY_VERSION)
end

group :test do
  gem 'oj', '> 3'
  gem 'json', '< 2.0' if ruby_version?('< 2.0')
  gem 'simplecov'
  gem 'rack', '< 2.0' if ruby_version?('< 2.2.2')
  gem 'rack-test'
  gem 'rake'
  gem 'pry'
  gem 'rb_sys', '~> 0.9'
  gem 'rspec'
  gem 'rubocop', ruby_version?('< 2.0') ? '< 0.42' : nil
  gem 'tins', '< 1.7' if ruby_version?('< 2.0')
end

# Fork to support Ruby 3.3.0-preview2, can be removed once Ruby 3.3.0 is released
gem 'rake-compiler-dock', github: "basecamp/rake-compiler-dock", branch: "explore-3.3.0-preview2"
