stages:
  - test
  - build_gems
  - smoke_test
  - deploy
  - release

.install-rust: &install-rust
  - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --quiet --default-toolchain=1.65.0 --profile=minimal
  - source "$HOME/.cargo/env"

.install-ruby: &install-ruby
  - ruby -v
  - gem install bundler --no-document
  - bundle --version
  - bundle config --local path vendor
  - bundle install -j $(nproc)

.install-ruby-and-compile: &install-ruby-and-compile
  - *install-ruby
  - bundle exec rake compile

.test-ruby: &test-ruby
  - bundle exec rake spec
  - prometheus_rust_mmaped_file=false bundle exec rake spec

cache:
  key: ${CI_JOB_IMAGE}
  paths:
    - vendor/ruby

before_script:
  - apt-get update
  - apt-get install -y curl ruby ruby-dev build-essential llvm-dev libclang-dev clang
  - *install-rust
  - *install-ruby-and-compile

.test-job: &test-job
  image: ruby:${RUBY_VERSION}
  stage: test
  variables:
    prometheus_multiproc_dir: tmp/
    BUILDER_IMAGE_REVISION: "4.9.1"
    RUBY_VERSION: "2.7"
  script:
    - bundle exec rake spec
    - prometheus_rust_mmaped_file=false bundle exec rake spec
    - cd ext/fast_mmaped_file_rs && cargo nextest run
  artifacts:
    paths:
      - coverage/

ruby:
  <<: *test-job
  before_script:
    - apt-get update
    - apt-get install -y llvm-dev libclang-dev clang
    - *install-rust
    - *install-ruby-and-compile
    - curl -LsSf https://get.nexte.st/0.9/linux | tar zxf - -C ${CARGO_HOME:-~/.cargo}/bin
  parallel:
    matrix:
      - RUBY_VERSION: ["2.7", "3.0", "3.1", "3.2", "3.3.0-preview2"]

ruby-head:
  <<: *test-job
  image: rubocophq/ruby-snapshot:latest
  before_script:
    - apt-get update
    - apt-get install -y llvm-dev libclang-dev clang
    - *install-rust
    - *install-ruby-and-compile
    - curl -LsSf https://get.nexte.st/0.9/linux | tar zxf - -C ${CARGO_HOME:-~/.cargo}/bin

builder:centos_7:
  <<: *test-job
  before_script:
    - source /opt/rh/llvm-toolset-7/enable
    - *install-ruby-and-compile
  script:
    - *test-ruby
  image: registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/centos_7:${BUILDER_IMAGE_REVISION}

builder:centos_8:
  <<: *test-job
  before_script:
    - *install-ruby-and-compile
  script:
    - *test-ruby
  image: registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/centos_8:${BUILDER_IMAGE_REVISION}

i386/debian:bullseye:
  <<: *test-job
  image: i386/debian:bullseye
  before_script:
    - apt-get update
    - apt-get install -y curl ruby ruby-dev build-essential llvm-dev libclang-dev clang git
    - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --quiet --default-toolchain=1.65.0 --profile=minimal --default-host=i686-unknown-linux-gnu
    - source "$HOME/.cargo/env"
    - *install-ruby-and-compile
    - curl -LsSf https://get.nexte.st/0.9/linux | tar zxf - -C ${CARGO_HOME:-~/.cargo}/bin
    - export LANG=C.UTF-8
  script:
    - *test-ruby

archlinux:ruby:3.0:
  <<: *test-job
  image: archlinux/archlinux:base
  before_script:
    - pacman -Sy --noconfirm git gcc clang make ruby ruby-bundler ruby-rdoc ruby-rake which grep gawk procps-ng glibc
    - *install-rust
    - *install-ruby-and-compile
    - curl -LsSf https://get.nexte.st/0.9/linux | tar zxf - -C ${CARGO_HOME:-~/.cargo}/bin
  script:
    - *test-ruby

fuzzbert:
  image: ruby:2.7
  variables:
    prometheus_multiproc_dir: tmp/
  script:
    - bundle exec fuzzbert fuzz/**/fuzz_*.rb --handler PrintAndExitHandler --limit 10000
    - prometheus_rust_mmaped_file=false bundle exec fuzzbert fuzz/**/fuzz_*.rb --handler PrintAndExitHandler --limit 10000

clang-format check:
  image: debian:bullseye-slim
  before_script:
    - apt-get update
    - apt-get install -y clang-format git
  script:
    - find ext/ -name '*.[ch]' | xargs clang-format -style=file -i
    - git diff --exit-code

rustfmt check:
  image: debian:bullseye-slim
  before_script:
    - apt-get update
    - apt-get install -y curl
    - *install-rust
    - rustup component add rustfmt
  script:
    - cargo fmt --manifest-path ext/fast_mmaped_file_rs/Cargo.toml -- --check

clippy check:
  image: debian:bullseye-slim
  before_script:
    - apt-get update
    - apt-get install -y curl ruby ruby-dev build-essential llvm-dev libclang-dev clang git
    - *install-rust
    - rustup component add clippy
  script:
    - cargo clippy --manifest-path ext/fast_mmaped_file_rs/Cargo.toml

rspec Address-Sanitizer:
  image: ruby:3.1
  before_script:
    - apt-get update
    - apt-get install -y git build-essential curl llvm-dev libclang-dev clang
    - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --quiet --default-toolchain=nightly-2022-11-03 --profile=minimal
    - source "$HOME/.cargo/env"
    - rustup component add rust-src
    - *install-ruby
  allow_failure: true
  script:
    - RB_SYS_EXTRA_CARGO_ARGS='-Zbuild-std' CARGO_BUILD_TARGET=x86_64-unknown-linux-gnu bundle exec rake compile -- --enable-address-sanitizer
    - export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libasan.so.8
    - export ASAN_OPTIONS=atexit=true:verbosity=1
    - export LSAN_OPTIONS=suppressions=known-leaks-suppression.txt
    - prometheus_rust_mmaped_file=false bundle exec rspec > /dev/null
    - bundle exec rspec > /dev/null

parsing Address-Sanitizer:
  image: ruby:3.1
  before_script:
    - apt-get update
    - apt-get install -y git build-essential curl llvm-dev libclang-dev clang
    - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --quiet --default-toolchain=nightly-2022-11-03 --profile=minimal
    - source "$HOME/.cargo/env"
    - rustup component add rust-src
    - *install-ruby-and-compile
  allow_failure: true
  script:
    - bundle exec rspec
    - RB_SYS_EXTRA_CARGO_ARGS='-Zbuild-std' CARGO_BUILD_TARGET=x86_64-unknown-linux-gnu bundle exec rake compile -- --enable-address-sanitizer
    - DB_FILE=$(basename $(find tmp -name '*.db' | head -n 1))
    - test -n $DB_FILE
    - for ((i=1;i<=100;i++)); do cp tmp/$DB_FILE tmp/$i$DB_FILE; done
    - export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libasan.so.8
    - export ASAN_OPTIONS=atexit=true:verbosity=1
    - export LSAN_OPTIONS=suppressions=known-leaks-suppression.txt
    - bundle exec bin/parse -t tmp/*.db > /dev/null

pages:
  image: ruby:3.1
  stage: deploy
  dependencies:
    - "ruby: [3.1]"
  script:
    - mv coverage/ public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master

gems:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  stage: build_gems
  needs: []
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - apk add ruby ruby-dev git make gcc g++
    - *install-ruby
  script:
    - bundle exec rake gem:${TARGET_PLATFORM}
  parallel:
    matrix:
      - TARGET_PLATFORM: ["aarch64-linux", "arm64-darwin", "x86_64-darwin", "x86_64-linux"]
  artifacts:
    paths:
      - pkg/*.gem
  only:
    - tags

gem_smoke_test:
  stage: build_gems
  image: ruby:3.1
  needs: ["gems: [x86_64-linux]"]
  before_script:
    - ruby -v
  script:
    - gem install pkg/prometheus-client-mmap-*-x86_64-linux.gem
    - echo "Checking if Rust extension is available..."
    - ruby -r 'prometheus/client/helper/loader' -e "exit 1 unless Prometheus::Client::Helper::Loader.rust_impl_available?"
  cache: []
  only:
    - tags

release:
  stage: release
  image: ruby:3.1
  before_script:
    - gem -v
  script:
    - ls -al pkg/*.gem
    - tools/deploy-rubygem.sh
  cache: []
  only:
    - tags
